# -*- coding: utf-8 -*-
# ========================================================
# Composant : py_framework
# Fichier   : cfg_output.py
# Date Créa : 27/07/2016
# Auteur    : houlder.hambalaye
# Version   : 01.00.00
# Date liv  : 2016-08-03
# ========================================================
# =====================================================================
#                            CODE DE SORTIE
# =====================================================================
# Erreur non bloquante pour le traitement (code inférieur à 500)
errVacuum = 101
errAnalyse = 102

# Erreur bloquante pour le traitement (code supérieur à 500)
errLog = 501
errBD = 502
errDate = 503
errLogistique = 504
errListeDate = 505
errGetFonction = 506
errGetBatch= 507
errClass= 508