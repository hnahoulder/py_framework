# ========================================================
# Composant : py_framework
# Fichier   : func.py
# Date Créa : 02/02/2017
# Auteur    : houlder.hambalaye
# Version   : 01.00.00
# Date liv  : 2016-08-03
# ========================================================
"""
Ce module contient les fonctions communes utilisées par tous les composants

"""

import copy
import sys
from py_framework.py.config.cfg_output import *


# Fichier log
def write_log(message, log):
    """
    Cette fonction permet d'écrire le message passé en paramètre dans un fichier log qui est aussi donné en paramètre

    :param message: Text à écrire
    :type message: str
    :param log: Nom du fichier dont lequel le message est à écrire
    :type log: logging
    """
    file = open(log, "a")
    file.write(message + "\n")
    file.close()


def get_fon_clef(myServer, fon_lib, prefixe, log_py):
    """
    Cette fonction retourne la clé primaire de fon_lib qui se trouve dans la table *gi_log.gi_fonction*

    :param myServer: Chaine de caractère contenant les informations sur le serveur de base de données.
    :type myServer: PgConnection
    :param fon_lib: Nom de la fonction à chercher
    :type fon_lib: str
    :param prefixe: Prefixe des schémas et des tables dans la base de données
    :type prefixe: str
    :param log_py: Nom du fichier de log. Ce fichier de log doit être une instance de  la classe logging
    :type log_py: logging
    :return: Identifiant de la fonciton (fon_clef)
    :rtype: int
    """
    try:
        myDb = copy.deepcopy(myServer)
        myDb.execute(
            """SELECT fon_clef from {$prefixe}log.{$prefixe}fonction WHERE fon_lib='{}' AND fon_date_sup IS NULL"""
            .replace('{$prefixe}', str(prefixe))
            .format(fon_lib))
        fon_clef = myDb.fetchone()
    except Exception as e:
        raise e
    if fon_clef:
        return fon_clef[0]
    else:
        log_py.error("Erreur {}. La fonction {} n'existe pas dans la table {$prefixe}log.{$prefixe}fonction"
                     .replace('{$prefixe}', str(prefixe))
                     .format(errGetFonction, fon_lib))
        sys.exit(errGetFonction)


def get_batch_clef(myServer, bat_lib, prefixe, log_py):
    """
    Cette fonction retourne la clé primaire du bat_clib qui se trouve dans *gi_log.gi_batch*.

    :param myServer: Chaine de caractère contenant les informations sur le serveur de base de données.
    :type myServer: PgConnection
    :param bat_lib: Nom du batch à chercher
    :type bat_lib: str
    :param prefixe: Prefixe des schémas et des tables dans la base de données
    :type prefixe: str
    :param log_py: Nom du fichier de log. Ce fichier de log doit être une instance de  la classe logging
    :type log_py: logging
    :return: Identifiant du batch(bat_clef)
    :rtype: int
    """
    try:
        myDb = copy.deepcopy(myServer)
        myDb.execute(
            """SELECT bat_clef from {$prefixe}log.{$prefixe}batch WHERE bat_lib='{}' AND bat_date_sup IS NULL"""
            .replace('{$prefixe}', str(prefixe))
            .format(bat_lib))
        bat_clef = myDb.fetchone()
    except Exception as e:
        raise e
    if bat_clef:
        return bat_clef[0]
    else:
        log_py.error("Erreur {}. Le composant {} n'existe pas dans la table {$prefixe}log.{$prefixe}batch"
                     .replace('{$prefixe}', str(prefixe))
                     .format(errGetBatch, bat_lib))
        sys.exit(errGetBatch)


def get_date_sem(myServer, prefixe):
    """
    Cette fonction retourne le numéro de semaine du traitement.

    :param myServer: Chaine de caractère contenant les informations sur le serveur de base de données
    :type myServer: PgConnection
    :param prefixe: Prefixe des schémas et des tables dans la base de données
    :type prefixe: str
    :return: Numéro de semaine
    :rtype: int

    """
    try:
        myDb = copy.deepcopy(myServer)
        myDb.execute("""SELECT sem_clef, date_donnee FROM {$prefixe}traitement.{$prefixe}semaine, {$prefixe}traitement.{$prefixe}date
        WHERE sem_date_deb <=(select date_donnee FROM {$prefixe}traitement.{$prefixe}date)
        AND sem_date_fin>=(select date_donnee FROM {$prefixe}traitement.{$prefixe}date)""".replace('{$prefixe}',
                                                                                                   str(prefixe)))
    except Exception as e:
        raise e
    return myDb.fetchone()


def insert_table_exe(myServer, prefixe, bat_clef, fon_clef):
    """
    Cette fonction permet d'inserer dans la table *gi_log.gi_execution* une trace de l'execution d'une fonction par un batch. En effet, cette table contient la durée d'execution d'une fonction.

    :param myServer: Chaine de caractère contenant les informations sur le serveur de base de données.
    :type myServer: PgConnection
    :param prefixe: Prefixe des schémas et des tables dans la base de données
    :type prefixe: str
    :param bat_clef: Identifiant du batch qui execute la fonction
    :type bat_clef: int
    :param fon_clef: Identifiant de la fonction à executer
    :type fon_clef: int
    :return: Identifiant de la trace de l'execution de la fonction (exe_clef)
    :rtype: int
    """
    try:
        myDb = copy.deepcopy(myServer)
        sem_date = get_date_sem(myDb, prefixe)
        myDb.execute("""INSERT INTO {$prefixe}log.{$prefixe}execution(bat_clef, fon_clef, sem_clef, pst_date_rec_sfr, exe_date_deb)
                        VALUES ({}, {}, {}, \'{}\', now()) RETURNING exe_clef"""
                     .replace('{$prefixe}', str(prefixe))
                     .format(bat_clef, fon_clef, sem_date[0], sem_date[1]))
        myDb.commit()
        exe_clef = myDb.fetchone()
    except Exception as e:
        raise e
    return exe_clef[0]


def update_table_exe(myServer, prefixe, bat_clef, fon_clef, exe_clef, exe_code_sortie):
    """
    Cette fonction met à jour le contenant de la table *gi_log.gi_execution*. Cette mise à jour se porte sur l'heure de fin de l'execution et le code retourné par la fonction.

    :param myServer: Chaine de caractère contenant les informations sur le serveur de base de données.
    :type myServer: PgConnection
    :param prefixe: Prefixe des schémas et des tables dans la base de données
    :type prefixe: str
    :param bat_clef: Identifiant du batch qui execute la fonction
    :type bat_clef: int
    :param fon_clef: Identifiant de la fonction à executer
    :type fon_clef: int
    :param exe_clef: Identifiant de la trace d'execution
    :type exe_clef: int
    :param exe_code_sortie: Code retourné par la fonction
    :type exe_code_sortie: int
    """
    try:
        myDb = copy.deepcopy(myServer)
        myDb.execute(
            """UPDATE {$prefixe}log.{$prefixe}execution SET exe_date_sup=now() WHERE exe_clef <> {} AND bat_clef = {} AND fon_clef = {} AND exe_date_deb::DATE = CURRENT_DATE """
            .replace('{$prefixe}', str(prefixe))
            .format(exe_clef, bat_clef, fon_clef))
        myDb.commit()
        myDb.execute(
            """UPDATE {$prefixe}log.{$prefixe}execution SET exe_date_fin=now(), exe_code_sortie={} WHERE exe_clef={}"""
            .replace('{$prefixe}', str(prefixe))
            .format(exe_code_sortie, exe_clef))
        myDb.commit()
    except Exception as e:
        raise e


# Calcul liste dates recues
def recherche_date_evenement(myServer, gi_date, prefixe, cutoff):
    # todo définir le cutoff
    """
    Cette fonction recherche toutes les dates des points remontés par les Distrio à la date {$prefixe}date.

    :param myServer: Chaine de caractère contenant les informations sur le serveur de base de données.
    :type myServer: PgConnection
    :param {$prefixe}date: Date du traitement
    :type {$prefixe}date: str
    """
    myDb = copy.deepcopy(myServer)
    try:
        myDb.execute(" SELECT {$prefixe}framework.{$prefixe}recherche_date_evenement(\'{}\', {}, \'{}\')"
                     .replace('{$prefixe}', str(prefixe))
                     .format(gi_date, cutoff, prefixe))
        myDb.commit()
    except Exception as e:
        raise e


def recherche_planification(dbPorteo, prefixe):
    # todo Documentation
    myDb = copy.deepcopy(dbPorteo)
    try:
        myDb.execute(
            " SELECT {$prefixe}framework.{$prefixe}recherche_planification(\'{$prefixe}\')".replace('{$prefixe}',
                                                                                                    str(prefixe)))
        myDb.commit()
        myDb.close()
    except Exception as e:
        raise e


def executer_requete(myServer, query):
    """

    :param myServer:
    :param query:
    :return:
    """
    myDb = copy.deepcopy(myServer)
    try:
        myDb.execute(query)
        myDb.commit()
    except Exception as e:
        raise e


def test():
    print("hello")
