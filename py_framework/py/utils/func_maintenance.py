# coding: utf-8
# ========================================================
# Composant : py_framework
# Fichier   : func_maintenance.py
# Date Créa : 27/07/2016
# Auteur    : houlder.hambalaye
# Version   : 01.00.00
# Date liv  : 2016-08-03
# ========================================================
"""
Ce module contient les fonctions de maintenance d'une base PostgreSQL:
    - ANALYSE (Voir :func:`pg_analyseTable`)
    - VACUUM (Voir :func:`pg_vacuumTable`)
    - VACUUM FULL (Voir :func:`pg_vacuumFullTable`)

"""
import copy

from py_framework.py.config.cfg_output import errAnalyse, errVacuum


def pg_vacuumTable(myServer,myTable,log):
    """
    Cette fonction fait une opération de maintenance de VACUUM et d'ANALYSE sur une table.

    :param myServer: Chaine de caractère contenant les informations sur le serveur de base de données.
    :type myServer: PgConnection
    :param myTable: Nom de la table à maintenir.
    :type myTable: str
    :param log: Nom du fichier de log. Ce fichier de log doit être une instance de  la classe logging.
    :type log: logging

    :Example:

        >>> from py_framework.py.utils.func_maintenance import pg_vacuumTable
        >>> from py_framework.py.utils.class_connection import PgConnection

        >>> dbDistrio = PgConnection(myConnection, paramMem, log)
        >>> pg_vacuumTable(dbDistrio, 'gi_traitement.gi_date', log)
    """
    try:
        myDb = copy.deepcopy(myServer)
        old_isolation_level = myDb.isolation_level()
        myDb.set_isolation_level(0)
        myDb.execute("VACUUM ANALYSE " + myTable)
        myDb.commit()
        myDb.set_isolation_level(old_isolation_level)
        myDb.commit()
        myDb.close()
    except Exception as e:
        log.warning(
            "Erreur {} lors de l'opération de maintenance VACUUM sur la table {} -- {}".format(errVacuum, myTable, e))


def pg_vacuumFullTable(myServer,myTable,log):
    """
    Cette fonction fait une opération de maintenance de VACUUM FULL et d'ANALYSE sur une table.

    :param myServer: Chaine de caractère contenant les informations sur le serveur de base de données.
    :type myServer: PgConnection
    :param myTable: Nom de la table à maintenir.
    :type myTable: str
    :param log: Nom du fichier de log. Ce fichier de log doit être une instance de  la classe logging.
    :type log: logging

    :Example:

        >>> from py_framework.py.utils.func_maintenance import pg_vacuumFullTable
        >>> from py_framework.py.utils.class_connection import PgConnection

        >>> dbDistrio = PgConnection(myConnection, paramMem, log)
        >>> pg_vacuumFullTable(dbDistrio, 'gi_traitement.gi_date', log)
    """
    try:
        myDb = copy.deepcopy(myServer)
        old_isolation_level = myDb.isolation_level()
        myDb.set_isolation_level(0)
        myDb.execute("VACUUM FULL ANALYSE " + myTable)
        myDb.commit()
        myDb.set_isolation_level(old_isolation_level)
        myDb.commit()
        myDb.close()
    except Exception as e:
        log.warning(
            "Erreur {} lors de l'opération de maintenance VACUUM sur la table {} -- {}".format(errVacuum, myTable, e))


def pg_analyseTable(myServer,myTable,log):
    """
    Cette fonction fait une opération de maintenance d'ANALYSE sur une table.

    :param myServer: Chaine de caractère contenant les informations sur le serveur de base de données.
    :type myServer: PgConnection
    :param myTable: Nom de la table à maintenir.
    :type myTable: str
    :param log: Nom du fichier de log. Ce fichier de log doit être une instance de  la classe logging.
    :type log: logging

    :Example:

        >>> from py_framework.py.utils.func_maintenance import pg_analyseTable
        >>> from py_framework.py.utils.class_connection import PgConnection

        >>> dbDistrio = PgConnection(myConnection, paramMem, log)
        >>> pg_analyseTable(dbDistrio, 'gi_traitement.gi_date', log)
    """
    try:
        myDb = copy.deepcopy(myServer)
        old_isolation_level = myDb.isolation_level()
        myDb.set_isolation_level(0)
        myDb.execute("ANALYSE " + myTable)
        myDb.commit()
        myDb.set_isolation_level(old_isolation_level)
        myDb.commit()
        myDb.close()
    except Exception as e:
        log.warning(
            "Erreur {} lors de l'opération de maintenance VACUUM sur la table {} -- {}".format(errAnalyse, myTable, e))
