# -*- coding: utf-8 -*-
# ========================================================
# Composant : py_po_traitement
# Fichier   : class_po_traitement.py
# Date Créa : 28/02/2017
# Auteur    : houlder.hambalaye
# Version   : 00.00.00
# Date liv  : 0000-00-00
# ========================================================
import sys

from py_framework.py.config.cfg_output import errClass


class TypeTemps:
    nomTemps = None
    tytClef = None
    heureDeb = None
    heureFin = None
    def __init__(self,nomTemps, tytClef, heureDeb, heureFin, log):
        """Initialisation de la classe"""
        try:
            self.nomTemps=nomTemps
            self.tytClef=tytClef
            self.heureDeb=heureDeb
            self.heureFin=heureFin
        except Exception as e:
            log.critical("Erreur lors de la création de l'objet type TypeTemps -- {} ".format(e, errClass))
            sys.exit(errClass)
