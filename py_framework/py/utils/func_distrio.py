# ========================================================
# Composant : py_framework
# Fichier   : func_distrio.py
# Date Créa : 27/07/2016
# Auteur    : houlder.hambalaye
# Version   : 01.00.00
# Date liv  : 2016-08-03
# ========================================================
"""
Ce module contient les fonctions communes utilisées par tous les composants Distrio

"""

import copy

def execute_query(myServer, query, myDict, myKey, log, errDict, error):
    """
    Cette fonction permet d'exécuter une requête sql. S'il y a une erreur lors de l'exécution, elle écrit les détails
    de cette erreur dans le fichier de log et ajoute au dictionnaire le code erreur correspondant.

    :param myServer: Chaine de caractère contenant les informations sur le serveur de base de données.
    :type myServer: PgConnection
    :param query: Chaine de caractère contenant une ou plusieurs requêtes SQL.
    :type query: str
    :param myDict: Dictionnaire pour ajouter la valeur retournée par la fonction
    :type myDict: dict
    :param myKey: Clé de la valeur retournée
    :type myKey: str
    :param log: Nom du fichier de log
    :type log: logging
    :param errDict: Dictionnaire contenant le code erreur retourné par la fonction
    :type errDict: dict
    :param error: Code erreur
    :type error: int
    """
    try:
        myDb = copy.deepcopy(myServer)
        myDb.execute(query)
        res = myDb.fetchone()
        myDict[myKey] = res[0]
    except Exception as e:
        log.warning("Erreur {} lors du comptage du {} pour la table de log -- {}".format(error, myKey, e))
        errDict[myKey] = error

