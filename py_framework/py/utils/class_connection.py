# ========================================================
# Composant : py_framework
# Fichier   : class_connection.py
# Date Créa : 27/07/2016
# Auteur    : houlder.hambalaye
# Version   : 01.00.00
# Date liv  : 2016-08-03
# ========================================================

import sys

import psycopg2

from py_framework.py.config.cfg_output import errBD


class PgConnection:
    """Classe pour la connexion à une base de données postgres

    :param myConnection: Chaine de caractère contenant les informations pour se connecter au serveur
    :type myConnection: str
    :param paramMem: Dictionnaire contenant les informations sur la taille des mémoires en Kilo Octet pour la maintenance et les requêtes.
    :type paramMem: dict
    :param log: Nom du fichier de log
    :type log: logging

    :Example:
        >>> myConnection = "host=myHost port=5454 user=toto password=pwd dbname=mabase"
        >>> paramMem = {'work_mem': 2048000, 'maintenance_work_mem': 2048000}
        >>> dbDistrio = PgConnection(myConnection, paramMem, log)


    """

    # définition de la méthode spéciale __init__ (constructeur)
    __conn = None
    __cur = None
    __log = None
    __serverParam = None
    __paramMem = None

    def __init__(self, myConnection, paramMem, log):
        """Initialisation de la classe"""
        try:
            self.__serverParam = str(myConnection)
            self.__paramMem = paramMem
            self.__conn = psycopg2.connect(self.__serverParam)
            self.__cur = self.__conn.cursor()
            self.__cur.execute("SET work_mem to {}".format(paramMem['work_mem']))
            self.__conn.commit()
            self.__cur.execute("SET maintenance_work_mem to {}".format(paramMem['maintenance_work_mem']))
            self.__conn.commit()
            self.__log = str(log)
        except Exception as e:
            log.critical("Erreur {} lors de la connexion à la base données -- {}".format(errBD, e))
            sys.exit(errBD)

    def __deepcopy__(self, other):
        return PgConnection(self.__serverParam, self.__paramMem, self.__log)

    def get_work_mem(self):
        """
                    Cette méthode  retourne la valeur du work_mem de la connexion

                    :Example:

                        >>> from py_framework.py.utils.class_connection import PgConnection

                        >>> dbDistrio = PgConnection(myConnection, paramMem, log)
                        >>> dbDistrio.show_work_mem()
                        work_mem: 64000kB, maintenance_work_mem: 64000kB

                    """
        self.__cur.execute("SHOW work_mem")
        work_mem = self.__cur.fetchone()
        self.__cur.execute("SHOW maintenance_work_mem")
        maintenance_work_mem = self.__cur.fetchone()
        return 'work_mem: {}, maintenance_work_mem: {}'.format(work_mem[0], maintenance_work_mem[0])

    def set_work_mem(self, val):
        """

        Cete méthode permet de définir la valeur du work_mem de la connexion

        :param val: Valeur de la mémoire en kB

        :Example:

                        >>> from py_framework.py.utils.class_connection import PgConnection

                        >>> dbDistrio = PgConnection(myConnection, paramMem, log)
                        >>> dbDistrio.set_work_mem('128000')
                        >>> dbDistrio.show_work_mem()
                        work_mem: 128000kB, maintenance_work_mem: 64000kB
        """
        self.__cur.execute("SET work_mem to {}".format(val))
        self.__conn.commit()

    def set_maintenance_work_mem(self, val):
        """
                Cete méthode permet de définir la valeur du maintenance_work_mem de la connexion

                :param val: Valeur de la mémoire en kB

                :Example:

                                >>> from py_framework.py.utils.class_connection import PgConnection

                                >>> dbDistrio = PgConnection(myConnection, paramMem, log)
                                >>> dbDistrio.set_maintenance_work_mem('128000')
                                >>> dbDistrio.show_work_mem()
                                work_mem: 64000kB, maintenance_work_mem: 128000kB
                """
        self.__cur.execute("SET maintenance_work_mem to {}".format(val))
        self.__conn.commit()

    def fetchall(self):
        """
            Cette méthode  retourne toutes les lignes restante d'un résultat d'une requête précédement éxécutée.
            Elle les retournes comme une liste de tuples. Une liste vide est retournée s'il n'y a plus d'enregistrement à retourner.

            :Example:

                >>> from py_framework.py.utils.class_connection import PgConnection

                >>> dbDistrio = PgConnection(myConnection, paramMem, log)
                >>> dbDistrio.execute("SELECT * FROM maTable)")
                >>> dbDistrio.fetchone()
                (3, 42, 'bar')
                >>> dbDistrio.fetchall()
                [(1, 100, "abc'def"), (2, None, 'dada')]

            """
        return self.__cur.fetchall()

    def set_isolation_level(self, val):
        self.__conn.set_isolation_level(val)

    def isolation_level(self):
        return self.__conn.isolation_level

    def fetchone(self):
        """
            Cette méthode  retourne un tuple du résultat d'une requête précédement éxécutée. A chaque fois qu'on
            l'appelle elle retourne une ligne et elle envoye la valeur nulle si aucune ligne n'est plus disponible.

            :Example:

                >>> from py_framework.py.utils.class_connection import PgConnection

                >>> dbDistrio = PgConnection(myConnection, paramMem, log)
                >>> dbDistrio.execute("SELECT * FROM maTable) WHERE id=3;")
                >>> dbDistrio.fetchone()
                (3, 42, 'bar')
            """
        return self.__cur.fetchone()

    def commit(self):
        """
            Cette méthode permet de valider toute transaction en attente dans la base de données.
            Par défaut, *psycopg2* ouvre une transaction avant l'exécution de la première commande SQL: si la méthode
            commit () n'est pas appelée, l'effet de toute manipulation de données seront perdues.

            :Example:

                >>> from py_framework.py.utils.class_connection import PgConnection

                >>> dbDistrio = PgConnection(myConnection, paramMem, log)
                >>> dbDistrio.execute("CREATE TABLE maTable(x integer,y CHARACTER VARYING);")
                >>> dbDistrio.commit()
                >>> dbDistrio.execute("INSERT INTO maTable(x,y) VALUES (42,'bar')")
                >>> # Si on ne fait pas de commit() à la fin du script, la table maTable existe mais sera vide

            """
        self.__conn.commit()

    def execute(self, query):
        """
            Cette méthode permet d'exécuter une requête SQL.

            :param query: Requête SQL.
            :type query: str

            :Example:

                >>> from py_framework.py.utils.class_connection import PgConnection

                >>> dbDistrio = PgConnection(myConnection, paramMem, log)
                >>> dbDistrio.execute("INSERT INTO maTable(x,y) VALUES (1,2);")

            """
        try:
            self.__cur.execute(query)
        except psycopg2.Error as e:
            raise e

    def close(self):
        """
            Cette méthode permet de fermet la connexion à la base de données. A partir de là, la connexion ne sera plus
            utilisable. Notez que la fermeture d'une connexion sans valider cette dernière supprime toutes les modifications
            dans la base de données seront perdues.

            :Example:

                >>> from py_framework.py.utils.class_connection import PgConnection

                >>> dbDistrio = PgConnection(myConnection, paramMem, log)
                >>> dbDistrio.execute("INSERT INTO maTable(x,y) VALUES (1,2);")
                >>> dbDistrio.commit()
                >>> dbDistrio.close()

            """
        self.__conn.close()

    def rollback(self):
        """Cette méthode annulle toutes modification apportées par les transactions en attente. Elle ferme la connexion
         avant de valider les transactions.

         :Example:

                >>> from py_framework.py.utils.class_connection import PgConnection

                >>> dbDistrio = PgConnection(myConnection, paramMem, log)
                >>> dbDistrio.execute("INSERT INTO maTable(x,y) VALUES (1,2);")
                >>> dbDistrio.rollback()"""
        self.__conn.rollback()

    def copy_expert(self, path, option):
        """Cette méthode copie les contenues d'une table vers un répertoire."""
        self.__cur.copy_expert(path, option)

    """def __del__(self):
        self.__conn.close()"""

