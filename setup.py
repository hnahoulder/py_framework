# setup.py

"""Setup for Simble Lib"""
from py_framework import __version__
import os


from setuptools import(
    setup,
    find_packages
)

if os.environ.get('CI_COMMIT_TAG'):
    version = os.environ['CI_COMMIT_TAG']
else:
    version = __version__

setup(
    name='py_framework',
    version=version,
    description='Python livrary for Proof of Concept',
    author='HH',
    packages=find_packages(
        exclude=[
            "*.tests",
            "*.tests.*",
            "tests.*",
            "tests",
            "log",
            "log.*",
            "*.log",
            "*.log.*"
        ]
    ),
    setup_requires = ['wheel', 'psycopg2']
)